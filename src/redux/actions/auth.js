import jwtDecode from "jwt-decode";
import axios from "../../utils/axios";
import * as actions from "./actionType";

export const authStart = () => {
  return {
    type: actions.AUTH_START,
  };
};

export const authSuccess = (token, payload) => {
  return {
    type: actions.AUTH_SUCCESS,
    token,
    payload,
  };
};
export const authFail = (payload) => {
  return {
    type: actions.AUTH_FAIL,
    payload,
  };
};

export const logOut = () => {
  localStorage.clear();
  return {
    type: actions.LOG_OUT,
  };
};

export const authLogOut = (exTime) => {
  return (dispatch) => {
    setTimeout(() => {
      dispatch(logOut());
    }, exTime);
  };
};

export const checkChangePass = (payload) => {
  return {
    type: actions.CHECK_CHANGE_PASS,
    payload,
  };
};

export const loadingTrue = () => {
  return {
    type: actions.LOADING_TRUE,
  };
};

export const loadingFail = () => {
  return {
    type: actions.LOADING_FALSE,
  };
};

export const auth = (username, password) => {
  return (dispatch) => {
    dispatch(authStart());
    dispatch(loadingTrue());
    const dataLogin = {
      username,
      password,
    };
    axios
      .post(`${process.env.REACT_APP_BASE_URL}/auth/login`, dataLogin)
      .then((res) => {
        const user = jwtDecode(res.data.accessToken);
        const expriesTime = new Date(
          new Date().getTime() + res.data.expiresIn * 1000
        );
        localStorage.setItem("token", res.data.accessToken);
        localStorage.setItem("username", user.username);
        localStorage.setItem("email", user.email);
        localStorage.setItem("role", user.role);
        localStorage.setItem("expriesTime", expriesTime);
        localStorage.setItem("user", JSON.stringify(user));
        dispatch(authSuccess(res.data.accessToken, user));
        dispatch(authLogOut(res.data.expiresIn * 1000));
        dispatch(loadingFail());
      })
      .catch((err) => {
        dispatch(authFail("Tên đăng nhập hoặc mật khẩu không chính xác"));
        dispatch(loadingFail());
      });
  };
};

export const authCheckState = () => {
  return (dispatch) => {
    const token = localStorage.getItem("token");
    const user = JSON.parse(localStorage.getItem("user"));
    if (!token) {
      dispatch(logOut());
    } else {
      const expriesTime = new Date(localStorage.getItem("expriesTime"));
      if (new Date() >= expriesTime) {
        dispatch(logOut());
        alert("Hết phiên đăng nhập . Vui lòng đăng nhập lại !");
      } else {
        dispatch(checkChangePass(user.loginFirst));
        dispatch(authSuccess(token, user));
        dispatch(authLogOut(expriesTime.getTime() - new Date().getTime()));
      }
    }
  };
};

export const changePassword = (dataChangePass) => {
  return (dispatch) => {
    axios
      .put(
        `${process.env.REACT_APP_BASE_URL}/user/auth/changePassword`,
        dataChangePass
      )
      .then((res) => {
        alert(res.data.message);
        if (res.data.statusCode === 200) {
          dispatch(checkChangePass(true));
          alert("Vui lòng đăng nhập lại !");
          dispatch(logOut());
        }
      })
      .catch((err) => alert(err));
  };
};

export const authRegister = (username, email, closeModal) => {
  return (dispatch) => {
    dispatch(loadingTrue());
    const role = localStorage.getItem("role");
    const emailAdmin = role == 1 ? localStorage.getItem("email") : null;
    const dataRegister = {
      username,
      email,
      emailAdmin,
    };
    axios
      .post(`${process.env.REACT_APP_BASE_URL}/auth/registerByEmail`, dataRegister)
      .then((res) => {
        if (res.data.statusCode === 200) {
          alert(res.data.message);
          dispatch(loadingFail());
          closeModal();
        } else {
          alert(res.data.message);
          dispatch(loadingFail());
        }
      })
      .catch((err) => {
        alert(err);
      });
  };
};
