//Auth
export const AUTH_START = "AUTH_START";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_FAIL = "AUTH_FAIL";
export const LOG_OUT = "LOG_OUT";
export const CHECK_CHANGE_PASS = "CHECK_CHANGE_PASS";
export const LOADING_TRUE = "LOADING_TRUE";
export const LOADING_FALSE = "LOADING_FALSE";
export const LOADING_EMPLOYEE_TRUE = "LOADING_EMPLOYEE_TRUE";
export const LOADING_EMPLOYEE_FALSE = "LOADING_EMPLOYEE_FALSE";

//User
export const GET_USER = "GET_USER";
export const POST_USER = "POST_USER";
export const UPDATE_USER = "UPDATE_USER";
export const DELETE_USER = "DELETE_USER";

//Department
export const GET_DEPARTMENT = "GET_DEPARTMENT";
export const GET_ALL_DEPARTMENT = "GET_ALL_DEPARTMENT";
export const POST_DEPARTMENT = "POST_DEPARTMENT";
export const UPDATE_DEPARTMENT = "UPDATE_DEPARTMENT";
export const DELETE_DEPARTMENT = "DELETE_DEPARTMENT";

//Employee
export const GET_EMPLOYEE = "GET_EMPLOYEE";
export const POST_EMPLOYEE = "POST_EMPLOYEE";
export const UPDATE_EMPLOYEE = "UPDATE_EMPLOYEE";
export const DELETE_EMPLOYEE = "DELETE_EMPLOYEE";

//Pagination
export const TOTAL_PAGE = "TOTAL_PAGE";
export const GET_PAGE = "GET_PAGE";
export const GET_PER_PAGE = "GET_PER_PAGE";
export const DATA_PAGE = "DATA_PAGE";
export const DATA_FILTER = "DATA_FILTER";
