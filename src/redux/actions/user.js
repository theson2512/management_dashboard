import axios from "axios";
import { toast } from "react-toastify";
import * as actions from "./actionType";
import { getPage, getPerPage, totalPage } from "./pagination";

export const getData = (payload) => {
  return {
    type: actions.GET_USER,
    payload,
  };
};

export const updateData = (id, dataUpdate) => {
  return {
    type: actions.UPDATE_USER,
    payload: { id, ...dataUpdate },
  };
};

export const deleteData = (payload) => {
  return {
    type: actions.DELETE_USER,
    payload,
  };
};
export const loadingTrue = () => {
  return {
    type: actions.LOADING_TRUE,
  };
};

export const loadingFail = () => {
  return {
    type: actions.LOADING_FALSE,
  };
};

export const getUser = (page, limit) => {
  return (dispatch) => {
    console.log(`/user/paginate?page=${page}&limit=${limit}`)
    dispatch(loadingTrue());
    axios
      .get(
        `${process.env.REACT_APP_BASE_URL}/user/paginate?page=${page}&limit=${limit}`
      )
      .then((res) => {
        dispatch(totalPage(res.data.meta.totalPages));
        // dispatch(getPage(res.data.meta.currentPage));
        dispatch(getPerPage(res.data.meta.itemsPerPage));
        dispatch(getData(res.data.items));
        dispatch(loadingFail());
      })
      .catch((err) => console.log(err));
  };
};

export const deleteUser = (id) => {
  return (dispatch) => {
    axios
      .delete(`${process.env.REACT_APP_BASE_URL}/user/${id}`)
      .then((res) => {
        if (res.data.statusCode === 200) {
          dispatch(deleteData(id));
          toast.success("Xóa tài khoản thành công !", {
            position: toast.POSITION.TOP_RIGHT,
          });
        } else {
          toast.error("Xóa tài khoản thất bại !", {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      })
      .catch((err) => {
        toast.error("Xóa tài khoản thất bại !", {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };
};

export const updateUser = (id, username, email, role, closeModal) => {
  return (dispatch) => {
    dispatch(loadingTrue());
    const dataRegister = {
      username,
      email,
      role,
    };
    axios
      .put(`${process.env.REACT_APP_BASE_URL}/user/${id}`, dataRegister)
      .then((res) => {
        if (res.data.statusCode === 200) {
          dispatch(loadingFail());
          dispatch(updateData(id, dataRegister));
          closeModal();
          toast.success("Sửa thông tin tài khoản thành công !", {
            position: toast.POSITION.TOP_RIGHT,
          });
        } else {
          toast.error("Sửa thông tin tài khoản thất bại !", {
            position: toast.POSITION.TOP_RIGHT,
          });
          dispatch(loadingFail());
        }
      })
      .catch((err) => {
        toast.error("Sửa tài khoản thất bại !", {
          position: toast.POSITION.TOP_RIGHT,
        });
        dispatch(loadingFail());
      });
  };
};
