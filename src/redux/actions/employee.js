import { toast } from "react-toastify";
import axios from "../../utils/axios";
import * as actions from "./actionType";
import { dataFilter, getPage, getPerPage, totalPage } from "./pagination";

export const getData = (payload) => {
  return {
    type: actions.GET_EMPLOYEE,
    payload,
  };
};

export const addData = (payload) => {
  return {
    type: actions.POST_EMPLOYEE,
    payload,
  };
};

export const update = (id, dataUpdate) => {
  return {
    type: actions.UPDATE_EMPLOYEE,
    payload: { id, ...dataUpdate },
  };
};

export const deleteData = (payload) => {
  return {
    type: actions.DELETE_EMPLOYEE,
    payload,
  };
};

export const loadingTrue = () => {
  return {
    type: actions.LOADING_TRUE,
  };
};

export const loadingFail = () => {
  return {
    type: actions.LOADING_FALSE,
  };
};

export const getEmployee = (page, limit, data) => {
  return (dispatch) => {
    console.log(`employee/paginate?page=${page}&limit=${limit}&nameEmployee=${data}`,'employee')
    dispatch(loadingTrue());
    if (data) {
      axios
        .get(
          `${process.env.REACT_APP_BASE_URL}/employee/paginate?page=${page}&limit=${limit}&nameEmployee=${data}`
        )
        .then((res) => {
          dispatch(getData(res.data.items));
          dispatch(totalPage(res.data.meta.totalPages));
          dispatch(getPage(Number(res.data.meta.currentPage)));
          dispatch(getPerPage(res.data.meta.itemsPerPage));
          dispatch(loadingFail());
        })
        .catch((err) => console.log(err));
      // return;
    } else {
      axios
        .get(
          `${process.env.REACT_APP_BASE_URL}/employee/paginate?page=${page}&limit=${limit}`
        )
        .then((res) => {
          dispatch(totalPage(res.data.meta.totalPages));
          dispatch(getPage(res.data.meta.currentPage));
          dispatch(getPerPage(res.data.meta.itemsPerPage));
          dispatch(getData(res.data.items));
          dispatch(loadingFail());
        })
        .catch((err) => console.log(err));
    }
  };
};

export const addEmployee = (
  nameEmployee,
  photo,
  jobTitle,
  cellPhone,
  email,
  managerId,
  closeModal
) => {
  return (dispatch) => {
    dispatch(loadingTrue());
    const formData = new FormData();
    formData.append("nameEmployee", nameEmployee);
    formData.append("photo", photo);
    formData.append("jobTitle", jobTitle);
    formData.append("cellPhone", cellPhone);
    formData.append("email", email);
    formData.append("managerId", managerId);
    axios
      .post(`${process.env.REACT_APP_BASE_URL}/employee`, formData)
      .then((res) => {
        if (res.data.statusCode === 201) {
          dispatch(loadingFail());
          dispatch(addData(res.data.dataSave));
          closeModal();
          toast.success("Thêm nhân viên thành công !", {
            position: toast.POSITION.TOP_RIGHT,
          });
        } else {
          toast.error("Thêm nhân viên thất bại !", {
            position: toast.POSITION.TOP_RIGHT,
          });
          dispatch(loadingFail());
        }
      })
      .catch((err) => {
        toast.error("Thêm nhân viên thất bại !", {
          position: toast.POSITION.TOP_RIGHT,
        });
        dispatch(loadingFail());
      });
  };
};

export const deleteEmployee = (id) => {
  return (dispatch) => {
    axios
      .delete(`${process.env.REACT_APP_BASE_URL}/employee/${id}`)
      .then((res) => {
        if (res.data.statusCode === 200) {
          dispatch(deleteData(id));
          toast.success("Xóa nhân viên thành công !", {
            position: toast.POSITION.TOP_RIGHT,
          });
        } else {
          toast.error("Xóa nhân viên thất bại !", {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      })
      .catch((err) => {
        toast.error("Xóa nhân viên thất bại !", {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };
};

export const updateEmployee = (
  id,
  nameEmployee,
  photo,
  jobTitle,
  cellPhone,
  email,
  managerId,
  closeModal
) => {
  return (dispatch) => {
    dispatch(loadingTrue());
    const formData = new FormData();
    formData.append("nameEmployee", nameEmployee);
    formData.append("photo", photo);
    formData.append("jobTitle", jobTitle);
    formData.append("cellPhone", cellPhone);
    formData.append("email", email);
    formData.append("managerId", managerId);
    axios
      .put(`${process.env.REACT_APP_BASE_URL}/employee/${id}`, formData)
      .then((res) => {
        if (res.data.statusCode === 200) {
          dispatch(loadingFail());
          dispatch(update(id, formData));
          closeModal();
          toast.success("Sửa thông tin nhân viên thành công !", {
            position: toast.POSITION.TOP_RIGHT,
          });
        } else {
          toast.error("Sửa thông tin nhân viên thất bại !", {
            position: toast.POSITION.TOP_RIGHT,
          });
          dispatch(loadingFail());
        }
      })
      .catch((err) => {
        toast.error("Xóa nhân viên thất bại !", {
          position: toast.POSITION.TOP_RIGHT,
        });
        dispatch(loadingFail());
      });
  };
};
