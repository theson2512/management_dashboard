import { toast } from "react-toastify";
import axios from "../../utils/axios";
import * as actions from "./actionType";
import { getPage, getPerPage, totalPage } from "./pagination";

export const getData = (payload) => {
  return {
    type: actions.GET_DEPARTMENT,
    payload,
  };
};
export const getAllData = (payload) => {
  return {
    type: actions.GET_ALL_DEPARTMENT,
    payload,
  };
};

export const addData = (payload) => {
  return {
    type: actions.POST_DEPARTMENT,
    payload,
  };
};

export const updateData = (id, dataUpdate) => {
  return {
    type: actions.UPDATE_DEPARTMENT,
    payload: { id, ...dataUpdate },
  };
};

export const deleteData = (payload) => {
  return {
    type: actions.DELETE_DEPARTMENT,
    payload,
  };
};

export const loadingTrue = () => {
  return {
    type: actions.LOADING_TRUE,
  };
};

export const loadingFail = () => {
  return {
    type: actions.LOADING_FALSE,
  };
};
export const loadingEmployeeTrue = () => {
  return {
    type: actions.LOADING_EMPLOYEE_TRUE,
  };
};

export const loadingEmployeeFail = () => {
  return {
    type: actions.LOADING_EMPLOYEE_FALSE,
  };
};

export const getDepartment = (page, limit) => {
  return (dispatch) => {
    console.log(`department/paginate?page=${page}&limit=${limit}`,'department')
    dispatch(loadingTrue());
    axios
      .get(
        `${process.env.REACT_APP_BASE_URL}/department/paginate?page=${page}&limit=${limit}`
      )
      .then((res) => {
        dispatch(totalPage(res.data.meta.totalPages));
        dispatch(getPage(res.data.meta.currentPage));
        dispatch(getPerPage(res.data.meta.itemsPerPage));
        dispatch(getData(res.data.items));
        dispatch(loadingFail());
      })
      .catch((err) => console.log(err));
  };
};
export const getAllDepartment = () => {
  return (dispatch) => {
    axios
      .get(`${process.env.REACT_APP_BASE_URL}/department`)
      .then((res) => {
        dispatch(getAllData(res.data));
      })
      .catch((err) => console.log(err));
  };
};

export const addDepartment = (nameDepartment, officePhone, closeModal) => {
  return (dispatch) => {
    dispatch(loadingTrue());
    const dataDepartment = {
      nameDepartment,
      officePhone,
    };
    axios
      .post(`${process.env.REACT_APP_BASE_URL}/department`, dataDepartment)
      .then((res) => {
        if (res.data.statusCode === 201) {
          dispatch(loadingFail());
          dispatch(addData(res.data.dataSave));
          closeModal();
          toast.success("Thêm phòng ban thành công !", {
            position: toast.POSITION.TOP_RIGHT,
          });
        } else {
          toast.error("Thêm phòng ban thất bại !", {
            position: toast.POSITION.TOP_RIGHT,
          });
          dispatch(loadingFail());
        }
      })
      .catch((err) => {
        toast.error("Thêm phòng ban thất bại !", {
          position: toast.POSITION.TOP_RIGHT,
        });
        dispatch(loadingFail());
      });
  };
};

export const deleteDepartment = (id) => {
  return (dispatch) => {
    axios
      .delete(`${process.env.REACT_APP_BASE_URL}/department/${id}`)
      .then((res) => {
        if (res.data.statusCode === 200) {
          dispatch(deleteData(id));
          toast.success("Xóa phòng ban thành công !", {
            position: toast.POSITION.TOP_RIGHT,
          });
        } else {
          toast.error("Xóa phòng ban thất bại !", {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      })
      .catch((err) => {
        toast.error("Xóa phòng ban thất bại !", {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };
};

export const updateDepartment = (
  id,
  nameDepartment,
  officePhone,
  closeModal
) => {
  return (dispatch) => {
    dispatch(loadingTrue());
    const dataDepartment = {
      nameDepartment,
      officePhone,
    };
    axios
      .put(`${process.env.REACT_APP_BASE_URL}/department/${id}`, dataDepartment)
      .then((res) => {
        if (res.data.statusCode === 200) {
          dispatch(loadingFail());
          dispatch(updateData(id, dataDepartment));
          closeModal();
          toast.success("Sửa thông tin phòng ban thành công !", {
            position: toast.POSITION.TOP_RIGHT,
          });
        } else {
          toast.error("Sửa thông tin phòng ban thất bại !", {
            position: toast.POSITION.TOP_RIGHT,
          });
          dispatch(loadingFail());
        }
      })
      .catch((err) => {
        toast.error("Xóa phòng ban thất bại !", {
          position: toast.POSITION.TOP_RIGHT,
        });
        dispatch(loadingFail());
      });
  };
};
