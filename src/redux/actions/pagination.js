import * as actionType from "./actionType";

export const totalPage = (payload) => {
  return {
    type: actionType.TOTAL_PAGE,
    payload,
  };
};

export const getPage = (payload) => {
  return {
    type: actionType.GET_PAGE,
    payload,
  };
};

export const getPerPage = (payload) => {
  return {
    type: actionType.GET_PER_PAGE,
    payload,
  };
};

export const dataPage = (payload) => {
  return {
    type: actionType.DATA_PAGE,
    payload,
  };
};
export const dataFilter = (payload) => {
  return {
    type: actionType.DATA_FILTER,
    payload,
  };
};
