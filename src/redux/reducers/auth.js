import * as actions from "../actions/actionType";

const authData = {
  token: null,
  message: "",
  checkChangePass: false,
  loading: false,
};

export const auth = (state = authData, action) => {
  switch (action.type) {
    case actions.AUTH_SUCCESS:
      console.log(action.payload, "payload");
      return {
        ...state,
        token: action.token,
        role: action.payload.role,
        checkChangePass: action.payload.loginFrist,
        message: "",
      };
    case actions.AUTH_START:
      return {
        ...state,
      };
    case actions.AUTH_FAIL:
      return {
        ...state,
        message: action.payload,
      };
    case actions.LOG_OUT:
      return {
        ...state,
        token: null,
        role: 0,
        checkChangePass: false,
        message: "",
      };
    case actions.CHECK_CHANGE_PASS:
      return {
        ...state,
        checkChangePass: action.payload,
      };
    case actions.LOADING_TRUE:
      return {
        ...state,
        loading: true,
      };
    case actions.LOADING_FALSE:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};
