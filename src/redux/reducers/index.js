import { combineReducers } from "redux";
import { auth } from "./auth";
import { department } from "./department";
import { pagination } from "./pagination";
import { employee } from "./employee";
import { user } from "./user";

export const reducers = combineReducers({
  auth,
  department,
  pagination,
  employee,
  user,
});
