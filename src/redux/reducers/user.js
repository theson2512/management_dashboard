import * as actions from "../actions/actionType";

const userData = {
  userList: [],
  loading: false,
};

export const user = (state = userData, action) => {
  switch (action.type) {
    case actions.GET_USER:
      return {
        ...state,
        userList: [...action.payload],
      };
    case actions.UPDATE_USER:
      return {
        ...state,
        userList: state.userList.map((x) =>
          x.id === action.payload.id ? action.payload : x
        ),
      };
    case actions.DELETE_USER:
      return {
        ...state,
        userList: state.userList.filter((x) => x.id !== action.payload),
      };
    case actions.LOADING_TRUE:
      return {
        ...state,
        loading: true,
      };
    case actions.LOADING_FALSE:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};
