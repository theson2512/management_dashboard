import * as actions from "../actions/actionType";

const employeeData = {
  employeeList: [],
  loading: false,
};

export const employee = (state = employeeData, action) => {
  switch (action.type) {
    case actions.GET_EMPLOYEE:
      return {
        ...state,
        employeeList: [...action.payload],
      };
    case actions.POST_EMPLOYEE:
      return {
        ...state,
        employeeList: [...state.employeeList, action.payload],
      };
    case actions.UPDATE_EMPLOYEE:
      console.log(action.payload, "payload");
      return {
        ...state,
        employeeList: state.employeeList.map((x) =>
          x.id === action.payload.id ? action.payload : x
        ),
      };
    case actions.DELETE_EMPLOYEE:
      return {
        ...state,
        employeeList: state.employeeList.filter((x) => x.id !== action.payload),
      };
    case actions.LOADING_TRUE:
      return {
        ...state,
        loading: true,
      };
    case actions.LOADING_FALSE:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};
