import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
require('dotenv').config()

ReactDOM.render(
  <Provider store={store}>
    <App />,
    <ToastContainer style={{ marginTop: 50 }} />
  </Provider>
  ,
  document.getElementById('root')
);
