import Pagination from "@material-ui/lab/Pagination";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { getPage } from "../../redux/actions/pagination";

export default function PaginationTable() {
  const history = useHistory();
  const dispatch = useDispatch();
  const page = useSelector((state) => state.pagination.currentPage);
  const perPage = useSelector((state) => state.pagination.perPage);
  const totalPage = useSelector((state) => state.pagination.totalPage);
  
  const handleChange = (event, value) => {
    history.push({
      search: `?page=${value}&limit=${perPage}`,
    });
    dispatch(getPage(value));
  };

  return (
    <div
      style={{
        display: "flex",
        margin: "20px 20px ",
        justifyContent: "flex-end",
        padding: "10px 0",
      }}
    >
      <Pagination
        count={totalPage}
        page={page}
        onChange={handleChange}
        color="primary"
        showFirstButton
        showLastButton
      />
    </div>
  );
}
