import React from "react";
import { useSelector } from "react-redux";
import PopupChangePass from "../../UI/Popup/ChangePassword/PopupChangePass";
import classes from "./Account.module.css";
import ResultTableUser from "./ResultTableUser/ResultTableUser";

const Account = () => {
  const checkChangePass = useSelector((state) => state.auth.checkChangePass);
  return (
    <>
      {checkChangePass && <PopupChangePass title="Đổi mật khẩu" />}
      <div>
        <div className={classes.container}>
          <h4>Xin chào {localStorage.getItem("username")} !!!!</h4>
        </div>
        <ResultTableUser />
      </div>
    </>
  );
};

export default Account;
