import { Button, ButtonGroup } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getPage } from "../../../redux/actions/pagination";
import { deleteUser, getUser } from "../../../redux/actions/user";
import PopupAddUser from "../../../UI/Popup/User/PopupAddUser";
import PopupEditUser from "../../../UI/Popup/User/PopupEditUser";
import Spinner from "../../../UI/Spinner/Spinner";
import classes from "../Account.module.css";

function DataResult(props) {
  return props.dataUser.map((item, index) => {
    index++;
    return (
      <TableRow key={item.id}>
        <TableCell align="center" component="th" scope="row">
          {index}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.username}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.email}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.role === 1 ? "Admin" : "User"}
        </TableCell>
        {props.role === 1 && (
          <TableCell align="center" component="th" scope="row">
            <ButtonGroup>
              <Button>
                <EditIcon
                  color="primary"
                  onClick={() => props.onUpdate(item.id)}
                />
              </Button>
              <Button>
                <DeleteIcon
                  color="secondary"
                  onClick={() => props.onDelete(item.id)}
                />
              </Button>
            </ButtonGroup>
          </TableCell>
        )}
      </TableRow>
    );
  });
}

function DataFail(props) {
  return (
    <TableRow>
      <TableCell
        align="center"
        component="th"
        scope="row"
        colSpan={props.role === 1 ? "5" : "4"}
      >
        Không có dữ liệu !
      </TableCell>
    </TableRow>
  );
}

const ResultTableUser = () => {
  const [show, setShow] = useState(false);
  const [showEdit, setShowEdit] = useState(false);
  const closeModal = () => setShow(false);
  const closeModalEdit = () => setShowEdit(false);
  const [dataEdit, setDataEdit] = useState(0);

  const dispatch = useDispatch();
  const dataUser = useSelector((state) => state.user.userList);
  const page = useSelector((state) => state.pagination.currentPage);
  const perPage = useSelector((state) => state.pagination.perPage);
  const role = useSelector((state) => state.auth.role);
  const loading = useSelector((state) => state.user.loading);

  const onDelete = (id) => {
    if (window.confirm("Bạn có muốn xóa tài khoản này không ?")) {
      dispatch(deleteUser(id));
    }
  };

  const onUpdate = (id) => {
    setShowEdit(true);
    setDataEdit(id);
  };

  const pageData = (currentPageNew, perPageNew) => {
    dispatch(getUser(currentPageNew, perPageNew));
  };

  useEffect(() => {
    pageData(page, perPage);
  }, [page]);

  // useEffect(() => {
  //   pageData(page, perPage);
  // },[page])

  let data;

  if (dataUser && dataUser.length) {
    data = (
      <DataResult
        dataUser={dataUser}
        role={role}
        onDelete={onDelete}
        onUpdate={onUpdate}
      />
    );
  } else {
    data = <DataFail role={role} />;
  }

  return (
    <div className={classes.containerUser}>
      {role === 1 && (
        <Button
          onClick={() => setShow(true)}
          className={classes.customBtn}
          style={{ color: "white", padding: "5px 30px", marginBottom: 10 }}
        >
          Tạo tài khoản mới{" "}
        </Button>
      )}
      {show && (
        <PopupAddUser
          title="Tạo tài khoản mới"
          open={show}
          closeModal={closeModal}
        />
      )}
      {showEdit && (
        <PopupEditUser
          title="Thay đổi thông tin tài khoản"
          open={showEdit}
          closeModal={closeModalEdit}
          dataEdit={dataEdit}
        />
      )}

      {loading ? (
        <Spinner />
      ) : (
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">STT</TableCell>
                <TableCell align="center">Tên người dùng</TableCell>
                <TableCell align="center">Email</TableCell>
                <TableCell align="center">Quyền</TableCell>
                {role === 1 ? <TableCell align="center"></TableCell> : null}
              </TableRow>
            </TableHead>
            <TableBody>{data}</TableBody>
          </Table>
        </TableContainer>
      )}
    </div>
  );
};

export default ResultTableUser;
