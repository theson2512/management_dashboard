import { Button, ButtonGroup } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteDepartment,
  getDepartment,
} from "../../redux/actions/department";
import { getPage } from "../../redux/actions/pagination";
import PopupAddDepartment from "../../UI/Popup/Department/PopupAddDepartment";
import PopupEditDepartment from "../../UI/Popup/Department/PopupEditDepartment";
import PopupShowEmployee from "../../UI/Popup/Department/PopupShowEmployee";
import Spinner from "../../UI/Spinner/Spinner";
import classes from "./Department.module.css";

function DataResult(props) {
  return props.dataDepartment.map((item, index) => {
    index++;
    return (
      <TableRow key={item.id}>
        <TableCell align="center" component="th" scope="row">
          {index}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.nameDepartment}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.officePhone}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          <Button color="primary" onClick={() => props.onShowEmployee(item.id)}>
            Xem danh sách
          </Button>
        </TableCell>
        {props.role === 1 ? (
          <TableCell align="center" component="th" scope="row">
            <ButtonGroup>
              <Button>
                <EditIcon
                  color="primary"
                  onClick={() => props.onUpdate(item.id)}
                />
              </Button>
              <Button>
                <DeleteIcon
                  color="secondary"
                  onClick={() => props.onDelete(item.id)}
                />
              </Button>
            </ButtonGroup>
          </TableCell>
        ) : null}
      </TableRow>
    );
  });
}

function DataFail(props) {
  return (
    <TableRow>
      <TableCell
        align="center"
        component="th"
        scope="row"
        colSpan={props.role == 1 ? "5" : "4"}
      >
        Không có dữ liệu !
      </TableCell>
    </TableRow>
  );
}

const Department = () => {
  const [show, setShow] = useState(false);
  const [showEdit, setShowEdit] = useState(false);
  const [showListEmployee, setShowListEmployee] = useState(false);
  const closeModal = () => setShow(false);
  const closeModalEdit = () => setShowEdit(false);
  const closeModalShowEmployee = () => setShowListEmployee(false);
  const [dataEdit, setDataEdit] = useState(0);
  const [dataShow, setDataShow] = useState(0);

  const dispatch = useDispatch();
  const dataDepartment = useSelector(
    (state) => state.department.departmentList
  );
  const page = useSelector((state) => state.pagination.currentPage);
  const perPage = useSelector((state) => state.pagination.perPage);
  const role = useSelector((state) => state.auth.role);
  const loading = useSelector((state) => state.department.loading);

  const onDelete = (id) => {
    if (window.confirm("Bạn có muốn xóa phòng ban này không ?")) {
      dispatch(deleteDepartment(id));
    }
  };

  const onUpdate = (id) => {
    setShowEdit(true);
    setDataEdit(id);
  };

  const onShowEmployee = (id) => {
    setShowListEmployee(true);
    setDataShow(id);
  };

  const pageData = async (currentPageNew, perPageNew) => {
    await dispatch(getDepartment(currentPageNew, perPageNew));
  };

  useEffect(() => {
    pageData(page, perPage);
    console.log(page,'pageChangeDepartment')
  }, [page]);

  let data;

  if (dataDepartment && dataDepartment.length) {
    data = (
      <DataResult
        dataDepartment={dataDepartment}
        role={role}
        onDelete={onDelete}
        onUpdate={onUpdate}
        onShowEmployee={onShowEmployee}
      />
    );
  } else {
    data = <DataFail role={role} />;
  }

  return (
    <div className={classes.container}>
      {role === 1 ? (
        <Button
          onClick={() => setShow(true)}
          className={classes.customBtn}
          style={{ color: "white", padding: "5px 30px", marginBottom: 10 }}
        >
          Thêm phòng ban
        </Button>
      ) : null}
      {show ? (
        <PopupAddDepartment
          title="Thêm phòng ban"
          open={show}
          closeModal={closeModal}
        />
      ) : null}
      {showEdit ? (
        <PopupEditDepartment
          title="Thay đổi thông tin phòng ban"
          open={showEdit}
          closeModal={closeModalEdit}
          dataEdit={dataEdit}
        />
      ) : null}
      {showListEmployee ? (
        <PopupShowEmployee
          open={showListEmployee}
          closeModal={closeModalShowEmployee}
          dataShow={dataShow}
        />
      ) : null}
      {loading ? (
        <Spinner />
      ) : (
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">STT</TableCell>
                <TableCell align="center">Tên văn phòng</TableCell>
                <TableCell align="center">Số điện thoại</TableCell>
                <TableCell align="center">Danh sách nhân viên</TableCell>
                {role === 1 ? <TableCell align="center"></TableCell> : null}
              </TableRow>
            </TableHead>
            <TableBody>{data}</TableBody>
          </Table>
        </TableContainer>
      )}
    </div>
  );
};

export default Department;
