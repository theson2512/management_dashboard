import { Button, ButtonGroup } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllDepartment } from "../../redux/actions/department";
import { deleteEmployee, getEmployee } from "../../redux/actions/employee";
import { dataPage, getPage } from "../../redux/actions/pagination";
import PopupAddEmployee from "../../UI/Popup/Employee/PopupAddEmployee";
import PopupEditEmployee from "../../UI/Popup/Employee/PopupEditEmployee";
import Spinner from "../../UI/Spinner/Spinner";
import classes from "./Employee.module.css";
import FilterEmployee from "./FilterEmployee";

function DataResult(props) {
  return props.dataEmployee.map((item, index) => {
    index++;
    return (
      <TableRow key={item.id}>
        <TableCell align="center" component="th" scope="row">
          {index}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.nameEmployee}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          <img
            src={`${process.env.REACT_APP_BASE_URL}/employee/${item.photo}`}
            className={classes.imageEmployee}
          />
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.jobTitle}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.cellPhone}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.email}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.manager?.nameDepartment}
        </TableCell>
        {props.role === 1 && (
          <TableCell align="center" component="th" scope="row">
            <ButtonGroup>
              <Button>
                <EditIcon
                  color="primary"
                  onClick={() => props.onUpdate(item.id)}
                />
              </Button>
              <Button>
                <DeleteIcon
                  color="secondary"
                  onClick={() => props.onDelete(item.id)}
                />
              </Button>
            </ButtonGroup>
          </TableCell>
        )}
      </TableRow>
    );
  });
}

function DataFail(props) {
  return (
    <TableRow>
      <TableCell
        align="center"
        component="th"
        scope="row"
        colSpan={props.role == 1 ? "8" : "7"}
      >
        Không có dữ liệu !
      </TableCell>
    </TableRow>
  );
}

const Employee = () => {
  const [show, setShow] = useState(false);
  const [showEdit, setShowEdit] = useState(false);
  const [dataEdit, setDataEdit] = useState(0);
  const closeModal = () => setShow(false);
  const closeModalEdit = () => setShowEdit(false);

  const dispatch = useDispatch();
  const dataEmployee = useSelector((state) => state.employee.employeeList);
  const page = useSelector((state) => state.pagination.currentPage);
  const perPage = useSelector((state) => state.pagination.perPage);
  const dataFilter = useSelector((state) => state.pagination.dataFilter);
  const role = useSelector((state) => state.auth.role);
  const loading = useSelector((state) => state.employee.loading);

  const onDelete = (id) => {
    if (window.confirm("Bạn có muốn xóa nhân viên này không ?")) {
      dispatch(deleteEmployee(id));
    }
  };

  const onUpdate = (id) => {
    setShowEdit(true);
    setDataEdit(id);
  };

  const pageData = (currentPageNew, perPageNew, dataFilter) => {
    dispatch(getEmployee(currentPageNew, perPageNew, dataFilter));
    dispatch(getAllDepartment());
  };

  // useEffect(() => {
  //   dispatch(getPage(1))
  //   pageData(1, perPage, "");
  // }, []);

  useEffect(() => {
    pageData(page,perPage,dataFilter)
    console.log(page,'pageChange')
  }, [page]);

  let data;

  if (dataEmployee && dataEmployee.length) {
    data = (
      <DataResult
        dataEmployee={dataEmployee}
        role={role}
        onDelete={onDelete}
        onUpdate={onUpdate}
      />
    );
  } else {
    data = <DataFail role={role} />;
  }

  return (
    <div className={classes.container}>
      {role === 1 && (
        <div
          className="row"
          style={{
            display: "flex",
            justifyContent: "space-between",
            margin: 0,
          }}
        >
          <Button
            onClick={() => setShow(true)}
            className={classes.customBtn}
            style={{ color: "white", padding: "5px 30px", marginBottom: 10 }}
          >
            Thêm nhân viên
          </Button>
          <FilterEmployee />
        </div>
      )}
      {show && (
        <PopupAddEmployee
          title="Thêm nhân viên"
          open={show}
          closeModal={closeModal}
        />
      )}
      {showEdit && (
        <PopupEditEmployee
          title="Thay đổi thông tin phòng ban"
          open={showEdit}
          closeModal={closeModalEdit}
          dataEdit={dataEdit}
        />
      )}
      {loading ? (
        <Spinner />
      ) : (
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">STT</TableCell>
                <TableCell align="center">Tên nhân viên</TableCell>
                <TableCell align="center">Ảnh</TableCell>
                <TableCell align="center">Chức vụ</TableCell>
                <TableCell align="center">Số điện thoại</TableCell>
                <TableCell align="center">Email</TableCell>
                <TableCell align="center">Văn phòng </TableCell>
                <TableCell align="center"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{data}</TableBody>
          </Table>
        </TableContainer>
      )}
    </div>
  );
};

export default Employee;
