import _ from "lodash";
import React, { useCallback, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getEmployee } from "../../../redux/actions/employee";
import { dataFilter } from "../../../redux/actions/pagination";

const FilterEmployee = () => {
  const [searchItem, setSearchItem] = useState("");
  const dispatch = useDispatch();
  const page = useSelector((state) => state.pagination.currentPage);
  const perPage = useSelector((state) => state.pagination.perPage);

  const fetchDataFilter = (value) => {
    if (value) {
      dispatch(dataFilter(value));
      dispatch(getEmployee(1, perPage, value));
    } else {
      dispatch(dataFilter(""));
      dispatch(getEmployee(1, perPage, ""));
    }
  };

  const searchDebounce = useCallback(
    _.debounce((nextValue) => fetchDataFilter(nextValue), 200),
    []
  );

  const handlerChangeFilter = (e) => {
    const { value } = e.target;
    setSearchItem(value);
    searchDebounce(value);
  };
  return (
    <div>
      <form>
        <input
          type="text"
          value={searchItem}
          onChange={handlerChangeFilter}
          className="form-control"
          placeholder="Tìm kiếm theo tên"
        />
      </form>
    </div>
  );
};

export default FilterEmployee;
