import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import "react-phone-number-input/style.css";
import { useDispatch } from "react-redux";
import visibility from "../../../assets/visibility.png";
import visibility_hide from "../../../assets/visibility_hide.png";
import { changePassword, checkChangePass, logOut } from "../../../redux/actions/auth";
import Spinner from "../../Spinner/Spinner";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 550,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 35px ",
    borderRadius: 15,
  },
  title: {
    display: "flex",
    justifyContent: "center",
    marginBottom: 20,
    marginTop: 10,
  },
  titleCSKH: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginTop: 10,
  },
  phoneHotline: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    backgroundColor: "rgb(113, 190, 226)",
    padding: 5,
    borderRadius: 5,
  },
  showPassword: {
    padding: "5px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    cursor: "pointer",
    marginLeft: "-45px",
  },
}));

const PopupChangePass = (props) => {
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const [open, setOpen] = useState(true);
  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);
  const [show2, setShow2] = useState(false);

  const dispatch = useDispatch();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data) => {
    const username = localStorage.getItem("username");
    const dataChangePass = {
      username: username,
      oldPass: data.oldPass,
      newPass: data.newPass,
      confirm: data.confirm,
    };
    await dispatch(changePassword(dataChangePass))
  };

  return (
    <Modal
      open={open}
      onClose={props.closeModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <div style={modalStyle} className={classes.paper}>
          <div className={classes.title}>
            <h4 style={{ fontWeight: "bold", color: "rgb(0, 89, 131)" }}>
              {props.title}
            </h4>
          </div>

          <div className="row pb-2">
            <div
              className="col-12 py-2"
              style={{
                color: "rgb(109, 109, 109)",
                justifyContent: "center",
                alignItems: "center",
                fontWeight: "bold",
              }}
            >
              Mật khẩu cũ *
            </div>
            <div className="col-12" style={{ display: "flex" }}>
              <input
                type={show ? "text" : "password"}
                name="oldPass"
                className="w-100 form-control focus-remove-shadow"
                {...register("oldPass", {
                  required: true,
                  minLength: 6,
                  maxLength: 20,
                })}
              />
              <span
                onClick={() => setShow(!show)}
                className={classes.showPassword}
              >
                {show ? (
                  <img src={visibility} alt="show" />
                ) : (
                  <img src={visibility_hide} alt="hide" />
                )}
              </span>
            </div>
            <div
              style={{
                width: "100%",
                height: 20,
                paddingLeft: 10,
                paddingTop: 5,
              }}
            >
              {errors.oldPass && (
                <p style={{ color: "red" }}>Tối thiểu 6 ký tự !!!</p>
              )}
            </div>
          </div>

          <div className="row pb-2">
            <div
              className="col-12 py-2"
              style={{
                color: "rgb(109, 109, 109)",
                justifyContent: "center",
                alignItems: "center",
                fontWeight: "bold",
              }}
            >
              Mật khẩu mới *
            </div>
            <div className="col-12" style={{ display: "flex" }}>
              <input
                type={show1 ? "text" : "password"}
                name="newPass"
                className="w-100 form-control focus-remove-shadow"
                {...register("newPass", {
                  required: true,
                  minLength: 6,
                  maxLength: 20,
                })}
              />
              <span
                onClick={() => setShow1(!show1)}
                className={classes.showPassword}
              >
                {show1 ? (
                  <img src={visibility} alt="show" />
                ) : (
                  <img src={visibility_hide} alt="hide" />
                )}
              </span>
            </div>
            <div
              style={{
                width: "100%",
                height: 20,
                paddingLeft: 10,
                paddingTop: 5,
              }}
            >
              {errors.newPass && (
                <p style={{ color: "red" }}>Tối thiểu 6 ký tự !!!</p>
              )}
            </div>
          </div>

          <div className="row pb-2">
            <div
              className="col-12 py-2"
              style={{
                color: "rgb(109, 109, 109)",
                justifyContent: "center",
                alignItems: "center",
                fontWeight: "bold",
              }}
            >
              Xác nhận lại mật khẩu mới *
            </div>
            <div className="col-12" style={{ display: "flex" }}>
              <input
                type={show2 ? "text" : "password"}
                name="confirm"
                className="w-100 form-control focus-remove-shadow"
                {...register("confirm", {
                  required: true,
                  minLength: 6,
                  maxLength: 20,
                })}
              />
              <span
                onClick={() => setShow2(!show2)}
                className={classes.showPassword}
              >
                {show2 ? (
                  <img src={visibility} alt="show" />
                ) : (
                  <img src={visibility_hide} alt="hide" />
                )}
              </span>
            </div>
            <div
              style={{
                width: "100%",
                height: 20,
                paddingLeft: 10,
                paddingTop: 5,
              }}
            >
              {errors.confirm && (
                <p style={{ color: "red" }}>Tối thiểu 6 ký tự !!!</p>
              )}
            </div>
          </div>

          <div className="py-3 row" style={{ display: "flex" }}>
            <div
              className="col-12 "
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <button type="submit" className="btn btn-info">
                Thay đổi mật khẩu
              </button>
            </div>
          </div>
        </div>
      </form>
    </Modal>
  );
};

export default PopupChangePass;
