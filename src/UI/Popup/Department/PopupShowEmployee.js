import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  loadingEmployeeFail,
  loadingEmployeeTrue,
} from "../../../redux/actions/department";
import axios from "../../../utils/axios";
import Spinner from "../../Spinner/Spinner";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 550,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 35px ",
    borderRadius: 15,
  },
  title: {
    display: "flex",
    justifyContent: "center",
    marginBottom: 20,
    marginTop: 10,
  },
  titleCSKH: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginTop: 10,
  },
  phoneHotline: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    backgroundColor: "rgb(113, 190, 226)",
    padding: 5,
    borderRadius: 5,
  },
}));

function DataResult(props) {
  return props.data.map((item, index) => {
    index++;
    return (
      <TableRow key={item.id}>
        <TableCell align="center" component="th" scope="row">
          {index}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.nameEmployee}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          <img
            src={`${process.env.REACT_APP_BASE_URL}/employee/${item.photo}`}
            width="80px"
            height="60px"
          />
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.jobTitle}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.cellPhone}
        </TableCell>
        <TableCell align="center" component="th" scope="row">
          {item.email}
        </TableCell>
      </TableRow>
    );
  });
}

function DataFail() {
  return (
    <TableRow>
      <TableCell align="center" component="th" scope="row" colSpan="6">
        Không có dữ liệu !
      </TableCell>
    </TableRow>
  );
}

const PopupShowEmployee = (props) => {
  const [dataEmployee, setDataEmployee] = useState([]);
  const dispatch = useDispatch();
  const loadingEmployee = useSelector(
    (state) => state.department.loadingEmployee
  );

  useEffect(() => {
    dispatch(loadingEmployeeTrue());
    axios
      .get(
        `${process.env.REACT_APP_BASE_URL}/department/managerId/${props.dataShow}`
      )
      .then((res) => {
        setDataEmployee(res.data);
        dispatch(loadingEmployeeFail());
      })
      .catch((err) => console.log(err));
  }, []);

  let data;

  if (dataEmployee && dataEmployee.length) {
    data = <DataResult data={dataEmployee} />;
  } else {
    data = <DataFail />;
  }

  return (
    <Modal
      open={props.open}
      onClose={props.closeModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      style={{ margin: 100, height: "750px", overflow: "auto" }}
    >
      <TableContainer component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center">STT</TableCell>
              <TableCell align="center">Tên nhân viên</TableCell>
              <TableCell align="center">Ảnh</TableCell>
              <TableCell align="center">Chức vụ</TableCell>
              <TableCell align="center">Số điện thoại</TableCell>
              <TableCell align="center">Email</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loadingEmployee ? (
              <TableCell align="center" colSpan="6">
                <Spinner />
              </TableCell>
            ) : (
              data
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </Modal>
  );
};

export default PopupShowEmployee;
