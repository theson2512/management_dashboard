import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import React, { useEffect, useState } from "react";
import PhoneInput, { parsePhoneNumber } from "react-phone-number-input";
import "react-phone-number-input/style.css";
import { useDispatch, useSelector } from "react-redux";
import { updateDepartment } from "../../../redux/actions/department";
import Spinner from "../../Spinner/Spinner";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 550,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 35px ",
    borderRadius: 15,
  },
  title: {
    display: "flex",
    justifyContent: "center",
    marginBottom: 20,
    marginTop: 10,
  },
  titleCSKH: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginTop: 10,
  },
  phoneHotline: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    backgroundColor: "rgb(113, 190, 226)",
    padding: 5,
    borderRadius: 5,
  },
}));

const PopupEditDepartment = (props) => {
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const [nameDepartment, setDepartment] = useState("");
  const [number, setNumber] = useState("");

  const department = useSelector((state) => state.department.departmentList);
  const loading = useSelector((state) => state.department.loading);
  const dispatch = useDispatch();

  const onSubmitData = (e) => {
    e.preventDefault();
    dispatch(
      updateDepartment(props.dataEdit, nameDepartment, number, props.closeModal)
    );
  };

  useEffect(() => {
    if (props.dataEdit) {
      const dataFake = department.find((x) => x.id === props.dataEdit);
      const phoneNumber = parsePhoneNumber(dataFake.officePhone);
      const dataNew = {
        nameDepartment: dataFake.nameDepartment,
        number: phoneNumber.nationalNumber,
      };
      setDepartment(dataNew.nameDepartment);
      setNumber(dataNew.number);
    }
  }, []);

  return (
    <Modal
      open={props.open}
      onClose={props.closeModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <form onSubmit={onSubmitData}>
        <div style={modalStyle} className={classes.paper}>
          <div className={classes.title}>
            <h4 style={{ fontWeight: "bold", color: "rgb(0, 89, 131)" }}>
              {props.title}
            </h4>
          </div>

          <div className="row pb-2">
            <div
              className="col-12 py-2"
              style={{
                color: "rgb(109, 109, 109)",
                justifyContent: "center",
                alignItems: "center",
                fontWeight: "bold",
              }}
            >
              Tên phòng ban (*)
            </div>
            <div className="col-12">
              <input
                value={nameDepartment}
                type="text"
                name="nameDepartment"
                onChange={(e) => setDepartment(e.target.value)}
                className="form-control"
              />
            </div>
          </div>

          <div className="row pb-2 ">
            <div
              className="col-12 py-2"
              style={{ color: "rgb(109, 109, 109)", fontWeight: "bold" }}
            >
              Số điện thoại (*)
            </div>
            <div className="col-12">
              <PhoneInput
                placeholder="Enter phone number"
                value={number}
                onChange={setNumber}
              />
            </div>
          </div>

          <div className="py-3 row" style={{ display: "flex" }}>
            <div
              className="col-6 "
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <button className="btn btn-danger" onClick={props.closeModal}>
                Hủy
              </button>
            </div>
            <div
              className="col-6 "
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <button type="submit" className="btn btn-info">
                Sửa
              </button>
            </div>
          </div>
        </div>
        {loading && <Spinner />}
      </form>
    </Modal>
  );
};

export default PopupEditDepartment;
