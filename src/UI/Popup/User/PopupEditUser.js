import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateUser } from "../../../redux/actions/user";
import Spinner from "../../Spinner/Spinner";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 550,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 35px ",
    borderRadius: 15,
  },
  title: {
    display: "flex",
    justifyContent: "center",
    marginBottom: 20,
    marginTop: 10,
  },
  titleCSKH: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginTop: 10,
  },
  phoneHotline: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    backgroundColor: "rgb(113, 190, 226)",
    padding: 5,
    borderRadius: 5,
  },
}));

const PopupEditUser = (props) => {
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const [data, setData] = useState([]);

  const user = useSelector((state) => state.user.userList);
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.auth.loading);

  const onSubmitData = (e) => {
    e.preventDefault();
    dispatch(
      updateUser(
        props.dataEdit,
        data.username,
        data.email,
        Number(data.role),
        props.closeModal
      )
    );
  };

  const onChangeHandler = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    if (props.dataEdit) {
      const dataFake = user.find((x) => x.id === props.dataEdit);
      const { password, loginFirst, ...result } = dataFake;
      setData({ ...result });
    }
  }, []);

  return (
    <Modal
      open={props.open}
      onClose={props.closeModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <form onSubmit={onSubmitData}>
        <div style={modalStyle} className={classes.paper}>
          <div className={classes.title}>
            <h4 style={{ fontWeight: "bold", color: "rgb(0, 89, 131)" }}>
              {props.title}
            </h4>
          </div>

          <div className="row pb-2">
            <div
              className="col-12 py-2"
              style={{
                color: "rgb(109, 109, 109)",
                justifyContent: "center",
                alignItems: "center",
                fontWeight: "bold",
              }}
            >
              Tên tài khoản (*)
            </div>
            <div className="col-12">
              <input
                value={data.username}
                type="text"
                name="username"
                onChange={onChangeHandler}
                className="form-control"
              />
            </div>
          </div>

          <div className="row pb-2 ">
            <div
              className="col-12 py-2"
              style={{ color: "rgb(109, 109, 109)", fontWeight: "bold" }}
            >
              Email (*)
            </div>
            <div className="col-12">
              <input
                value={data.email}
                type="text"
                name="email"
                onChange={onChangeHandler}
                className="form-control"
              />
            </div>
          </div>
          <div className="row pb-2 ">
            <div
              className="col-12 py-2"
              style={{ color: "rgb(109, 109, 109)", fontWeight: "bold" }}
            >
              Quyền (*)
            </div>
            <div className="col-12">
              <select
                value={data.role}
                onChange={onChangeHandler}
                name="role"
                className="form-control"
              >
                <option value="1">Admin</option>
                <option value="0">User</option>
              </select>
            </div>
          </div>

          <div className="py-3 row" style={{ display: "flex" }}>
            <div
              className="col-6 "
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <button className="btn btn-danger" onClick={props.closeModal}>
                Hủy
              </button>
            </div>
            <div
              className="col-6 "
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <button type="submit" className="btn btn-info">
                Sửa
              </button>
            </div>
          </div>
        </div>
        {loading && <Spinner />}
      </form>
    </Modal>
  );
};

export default PopupEditUser;
