import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { authRegister } from "../../../redux/actions/auth";
import Spinner from "../../Spinner/Spinner";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 600,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 35px ",
    borderRadius: 15,
  },
  title: {
    display: "flex",
    justifyContent: "center",
    marginBottom: 20,
    marginTop: 10,
  },
  titleCSKH: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginTop: 10,
  },
  phoneHotline: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    backgroundColor: "rgb(113, 190, 226)",
    padding: 5,
    borderRadius: 5,
  },
}));

const PopupAddUser = (props) => {
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.auth.loading);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    dispatch(authRegister(data.username, data.email, props.closeModal));
  };

  return (
    <>
      <Modal
        open={props.open}
        onClose={props.closeModal}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <div style={modalStyle} className={classes.paper}>
            <div className={classes.title}>
              <h4 style={{ fontWeight: "bold", color: "rgb(0, 89, 131)" }}>
                {props.title}
              </h4>
            </div>

            <div className="row">
              <div className="col-12">
                <div className="row pb-2">
                  <div
                    className="col-12 py-2"
                    style={{
                      color: "rgb(109, 109, 109)",
                      justifyContent: "center",
                      alignItems: "center",
                      fontWeight: "bold",
                    }}
                  >
                    Tên đăng nhập *
                  </div>
                  <div className="col-12">
                    <input
                      type="text"
                      name="username"
                      className="form-control"
                      {...register("username", {
                        required: true,
                        minLength: 3,
                      })}
                    />
                    <div
                      style={{
                        width: "100%",
                        height: 20,
                        paddingLeft: 10,
                        paddingTop: 5,
                      }}
                    >
                      {errors.username && (
                        <p style={{ color: "red" }}>Tối thiểu 3 ký tự !!!</p>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-12">
                <div className="row pb-2">
                  <div
                    className="col-12 py-2"
                    style={{
                      color: "rgb(109, 109, 109)",
                      justifyContent: "center",
                      alignItems: "center",
                      fontWeight: "bold",
                    }}
                  >
                    Email *
                  </div>
                  <div className="col-12">
                    <input
                      type="text"
                      name="email"
                      className="form-control"
                      {...register("email", {
                        required: true,
                        pattern: /[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/i,
                      })}
                    />
                    <div
                      style={{
                        width: "100%",
                        height: 20,
                        paddingLeft: 10,
                        paddingTop: 5,
                      }}
                    >
                      {errors.email && (
                        <p style={{ color: "red" }}>
                          Vui lòng nhập đúng định dạng email !!!
                        </p>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="py-3 row" style={{ display: "flex" }}>
              <div
                className="col-6 "
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <button className="btn btn-danger" onClick={props.closeModal}>
                  Hủy
                </button>
              </div>
              <div
                className="col-6 "
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <button type="submit" className="btn btn-info">
                  Thêm
                </button>
              </div>
            </div>
          </div>
          <div>{loading && <Spinner />}</div>
        </form>
      </Modal>
    </>
  );
};

export default PopupAddUser;
