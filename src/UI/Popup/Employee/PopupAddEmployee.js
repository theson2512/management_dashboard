import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import PhoneInputWithCountry from "react-phone-number-input/react-hook-form";
import "react-phone-number-input/style.css";
import { useDispatch, useSelector } from "react-redux";
import { addEmployee } from "../../../redux/actions/employee";
import Spinner from "../../Spinner/Spinner";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 800,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 35px",
    borderRadius: 15,
  },
  title: {
    display: "flex",
    justifyContent: "center",
    marginBottom: 20,
    marginTop: 10,
  },
  titleCSKH: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginTop: 10,
  },
  phoneHotline: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    backgroundColor: "rgb(113, 190, 226)",
    padding: 5,
    borderRadius: 5,
  },
}));

const PopupAddEmployee = (props) => {
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const allDepartmentList = useSelector(
    (state) => state.department.allDepartment
  );
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.employee.loading);

  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
  } = useForm();

  const onSubmit = (data) => {
    dispatch(
      addEmployee(
        data.nameEmployee,
        data.photo[0],
        data.jobTitle,
        data.cellPhone,
        data.email,
        data.managerId,
        props.closeModal
      )
    );
  };

  return (
    <Modal
      open={props.open}
      onClose={props.closeModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <div style={modalStyle} className={classes.paper}>
          <div className={classes.title}>
            <h4 style={{ fontWeight: "bold", color: "rgb(0, 89, 131)" }}>
              {props.title}
            </h4>
          </div>

          <div className="row">
            <div className="col-6">
              <div className="row pb-2">
                <div
                  className="col-12 py-2"
                  style={{
                    color: "rgb(109, 109, 109)",
                    justifyContent: "center",
                    alignItems: "center",
                    fontWeight: "bold",
                  }}
                >
                  Tên nhân viên *
                </div>
                <div className="col-12">
                  <input
                    type="text"
                    name="nameEmployee"
                    className="form-control"
                    {...register("nameEmployee", { required: true })}
                  />
                </div>
              </div>
            </div>

            <div className="col-6">
              <div className="row pb-2">
                <div
                  className="col-12 py-2"
                  style={{
                    color: "rgb(109, 109, 109)",
                    justifyContent: "center",
                    alignItems: "center",
                    fontWeight: "bold",
                  }}
                >
                  Vị trí *
                </div>
                <div className="col-12">
                  <input
                    type="text"
                    name="jobTitle"
                    className="form-control"
                    {...register("jobTitle", { required: true })}
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-6">
              <div className="row pb-2">
                <div
                  className="col-12 py-2"
                  style={{
                    color: "rgb(109, 109, 109)",
                    justifyContent: "center",
                    alignItems: "center",
                    fontWeight: "bold",
                  }}
                >
                  Email *
                </div>
                <div className="col-12">
                  <input
                    type="text"
                    name="email"
                    className="form-control"
                    {...register("email", { required: true })}
                  />
                </div>
              </div>
            </div>

            <div className="col-6">
              <div className="row pb-2">
                <div
                  className="col-12 py-2"
                  style={{
                    color: "rgb(109, 109, 109)",
                    justifyContent: "center",
                    alignItems: "center",
                    fontWeight: "bold",
                  }}
                >
                  Văn phòng *
                </div>
                <div className="col-12">
                  <select
                    {...register("managerId", {
                      required: true,
                      maxLength: 11,
                    })}
                    className="w-100 form-control focus-remove-shadow"
                    style={{ boxShadow: "none !important" }}
                  >
                    {allDepartmentList.map((item) => (
                      <option key={item.id} value={item.id}>
                        {item.nameDepartment}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-6">
              <div className="row pb-2 ">
                <div
                  className="col-12 py-2"
                  style={{ color: "rgb(109, 109, 109)", fontWeight: "bold" }}
                >
                  Số điện thoại *
                </div>
                <div className="col-12">
                  <PhoneInputWithCountry
                    placeholder="Enter phone number"
                    name="cellPhone"
                    control={control}
                    rules={{ required: true }}
                  />
                </div>
              </div>
            </div>

            <div className="col-6">
              <div className="row pb-2">
                <div
                  className="col-12 py-2"
                  style={{
                    color: "rgb(109, 109, 109)",
                    justifyContent: "center",
                    alignItems: "center",
                    fontWeight: "bold",
                  }}
                >
                  Ảnh nhân viên *
                </div>
                <div className="col-12">
                  <input
                    accept="image/*"
                    id="contained-button-file"
                    type="file"
                    {...register("photo", { required: true })}
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="py-3 row" style={{ display: "flex" }}>
            <div
              className="col-6 "
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <button className="btn btn-danger" onClick={props.closeModal}>
                Hủy
              </button>
            </div>
            <div
              className="col-6 "
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <button type="submit" className="btn btn-info">
                Thêm
              </button>
            </div>
          </div>
        </div>
        {loading && <Spinner />}
      </form>
    </Modal>
  );
};

export default PopupAddEmployee;
