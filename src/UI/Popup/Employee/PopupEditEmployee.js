import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import React, { useEffect, useState } from "react";
import PhoneInput, { parsePhoneNumber } from "react-phone-number-input";
import "react-phone-number-input/style.css";
import { useDispatch, useSelector } from "react-redux";
import { updateDepartment } from "../../../redux/actions/department";
import { getEmployee, updateEmployee } from "../../../redux/actions/employee";
import Spinner from "../../Spinner/Spinner";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 800,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: "20px 35px ",
    borderRadius: 15,
  },
  title: {
    display: "flex",
    justifyContent: "center",
    marginBottom: 20,
    marginTop: 10,
  },
  titleCSKH: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginTop: 10,
  },
  phoneHotline: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    backgroundColor: "rgb(113, 190, 226)",
    padding: 5,
    borderRadius: 5,
  },
}));

const PopupEditEmployee = (props) => {
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const [number, setNumber] = useState("");
  const [data, setData] = useState([]);
  const allDepartmentList = useSelector(
    (state) => state.department.allDepartment
  );

  const employee = useSelector((state) => state.employee.employeeList);
  const loading = useSelector((state) => state.employee.loading);
  const dispatch = useDispatch();

  const onSubmitData = (e) => {
    e.preventDefault();
    dispatch(
      updateEmployee(
        props.dataEdit,
        data.nameEmployee,
        data.photo,
        data.jobTitle,
        number,
        data.email,
        data.managerId,
        props.closeModal
      )
    );
    setTimeout(() => {
      dispatch(getEmployee(1, 10, ""));
    }, 500);
  };

  const updateHandlerChanged = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };

  const updateFileChanged = (e) => {
    setData({
      ...data,
      photo: e.target.files[0],
    });
  };

  useEffect(() => {
    if (props.dataEdit) {
      const dataFake = employee.find((x) => x.id === props.dataEdit);
      const phoneNumber = parsePhoneNumber(dataFake.cellPhone);
      const dataNew = {
        nameEmployee: dataFake.nameEmployee,
        photo: dataFake.photo,
        jobTitle: dataFake.jobTitle,
        email: dataFake.email,
        managerId: dataFake.manager.id,
      };
      setData({ ...dataNew });
      setNumber(phoneNumber.nationalNumber);
    }
  }, []);

  return (
    <Modal
      open={props.open}
      onClose={props.closeModal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <form onSubmit={onSubmitData}>
        <div style={modalStyle} className={classes.paper}>
          <div className={classes.title}>
            <h4 style={{ fontWeight: "bold", color: "rgb(0, 89, 131)" }}>
              {props.title}
            </h4>
          </div>

          <div className="row">
            <div className="col-6">
              <div className="row pb-2">
                <div
                  className="col-12 py-2"
                  style={{
                    color: "rgb(109, 109, 109)",
                    justifyContent: "center",
                    alignItems: "center",
                    fontWeight: "bold",
                  }}
                >
                  Tên nhân viên *
                </div>
                <div className="col-12">
                  <input
                    type="text"
                    name="nameEmployee"
                    className="form-control"
                    value={data.nameEmployee}
                    onChange={updateHandlerChanged}
                  />
                </div>
              </div>
            </div>

            <div className="col-6">
              <div className="row pb-2">
                <div
                  className="col-12 py-2"
                  style={{
                    color: "rgb(109, 109, 109)",
                    justifyContent: "center",
                    alignItems: "center",
                    fontWeight: "bold",
                  }}
                >
                  Vị trí *
                </div>
                <div className="col-12">
                  <input
                    type="text"
                    name="jobTitle"
                    className="form-control"
                    value={data.jobTitle}
                    onChange={updateHandlerChanged}
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-6">
              <div className="row pb-2">
                <div
                  className="col-12 py-2"
                  style={{
                    color: "rgb(109, 109, 109)",
                    justifyContent: "center",
                    alignItems: "center",
                    fontWeight: "bold",
                  }}
                >
                  Email *
                </div>
                <div className="col-12">
                  <input
                    type="text"
                    name="email"
                    className="form-control"
                    value={data.email}
                    onChange={updateHandlerChanged}
                  />
                </div>
              </div>
            </div>

            <div className="col-6">
              <div className="row pb-2">
                <div
                  className="col-12 py-2"
                  style={{
                    color: "rgb(109, 109, 109)",
                    justifyContent: "center",
                    alignItems: "center",
                    fontWeight: "bold",
                  }}
                >
                  Văn phòng *
                </div>
                <div className="col-12">
                  <select
                    name="managerId"
                    className="w-100 form-control focus-remove-shadow"
                    style={{ boxShadow: "none !important" }}
                    value={data.managerId}
                    onChange={updateHandlerChanged}
                  >
                    {allDepartmentList.map((item) => (
                      <option key={item.id} value={item.id}>
                        {item.nameDepartment}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-6">
              <div className="row pb-2 ">
                <div
                  className="col-12 py-2"
                  style={{ color: "rgb(109, 109, 109)", fontWeight: "bold" }}
                >
                  Số điện thoại *
                </div>
                <div className="col-12">
                  <PhoneInput
                    placeholder="Enter phone number"
                    name="cellPhone"
                    value={number}
                    onChange={setNumber}
                  />
                </div>
              </div>
            </div>

            <div className="col-6">
              <div className="row pb-2">
                <div
                  className="col-12 py-2"
                  style={{
                    color: "rgb(109, 109, 109)",
                    justifyContent: "center",
                    alignItems: "center",
                    fontWeight: "bold",
                  }}
                >
                  Ảnh nhân viên *
                </div>
                <div className="col-12">
                  <input
                    type="file"
                    name="updateFileChanged"
                    value={data.updateFileChanged}
                    onChange={updateFileChanged}
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="py-3 row" style={{ display: "flex" }}>
            <div
              className="col-6 "
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <button className="btn btn-danger" onClick={props.closeModal}>
                Hủy
              </button>
            </div>
            <div
              className="col-6 "
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <button type="submit" className="btn btn-info">
                Sửa
              </button>
            </div>
          </div>
        </div>
        {loading && <Spinner />}
      </form>
    </Modal>
  );
};

export default PopupEditEmployee;
