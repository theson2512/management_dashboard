import AccountContainer from "./container/AccountContainer/AccountContainer";
import DepartmentContainer from "./container/DepartmentContainer/DepartmentContainer";
import EmployeeContainer from "./container/EmployeeContainer/EmployeeContainer";

const routers = [
    {
        path: "/",
        exact: true,
        main: () => <AccountContainer />
    },
    {
        path: "/Department",
        main: () => <DepartmentContainer />
    },
    {
        path: "/Employee",
        main: () => <EmployeeContainer />
    }
];

export default routers;