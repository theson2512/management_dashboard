import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import Login from "./component/Login/Login";
import Dashboard from "./container/dashboard/Dashboard";
import { authCheckState } from "./redux/actions/auth";

function App() {
  const dispatch = useDispatch();
  const token = useSelector((state) => state.auth.token);

  useEffect(() => {
    dispatch(authCheckState());
  }, []);

  let router = (
    <Switch>
      <Route exact path="/login" component={Login} />
      <Redirect to="/login" />
    </Switch>
  );
  if (token) {
    router = (
      <Switch>
        <Route exact path="/" component={Dashboard} />
        <Redirect to="/" />
      </Switch>
    );
  }
  return <Router>{router}</Router>;
}

export default App;
