import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import BarChartIcon from "@material-ui/icons/BarChart";
import DescriptionRoundedIcon from "@material-ui/icons/DescriptionRounded";
import HomeRoundedIcon from "@material-ui/icons/HomeRounded";
import LocalPharmacyRoundedIcon from "@material-ui/icons/LocalPharmacyRounded";
import PersonRoundedIcon from "@material-ui/icons/PersonRounded";
import React from "react";
import { NavLink } from "react-router-dom";

export const mainListItems = (
  <div>
    <NavLink to="/" style={{ textDecoration: "none", color: "#000000" }}>
      <ListItem button>
        <ListItemIcon>
          <HomeRoundedIcon />
        </ListItemIcon>
        <ListItemText primary="Quản lý người dùng" />
      </ListItem>
    </NavLink>

    <NavLink
      to="/Department"
      style={{ textDecoration: "none", color: "#000000" }}
    >
      <ListItem button>
        <ListItemIcon>
          <PersonRoundedIcon />
        </ListItemIcon>
        <ListItemText primary="Quản lý phòng ban" />
      </ListItem>
    </NavLink>

    <NavLink
      to="/Employee"
      style={{ textDecoration: "none", color: "#000000" }}
    >
      <ListItem button>
        <ListItemIcon>
          <PersonRoundedIcon />
        </ListItemIcon>
        <ListItemText primary="Quản lý nhân viên" />
      </ListItem>
    </NavLink>
  </div>
);
