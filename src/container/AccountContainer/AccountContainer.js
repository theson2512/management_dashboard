import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import Account from "../../component/Account/Account";
import PaginationTable from "../../component/Pagination/PaginationTable";
import { getPage } from "../../redux/actions/pagination";
import { getUser } from "../../redux/actions/user";

const AccountContainer = (props) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getUser(1, 10));
    dispatch(getPage(1))
  }, []);
  return (
    <div>
      <Account {...props} />
      <PaginationTable {...props} />
    </div>
  );
};

export default AccountContainer;
