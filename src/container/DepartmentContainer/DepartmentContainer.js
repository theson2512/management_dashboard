import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Department from "../../component/Department/Department";
import PaginationTable from "../../component/Pagination/PaginationTable";
import { getDepartment } from "../../redux/actions/department";
import { getPage } from "../../redux/actions/pagination";

export const DepartmentContainer = () => {
  const page = useSelector((state) => state.pagination.currentPage);
  const perPage = useSelector((state) => state.pagination.perPage);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getDepartment(1, perPage));
    dispatch(getPage(1));
  }, []);
  return (
    <div>
      <Department />
      <PaginationTable />
    </div>
  );
};

export default DepartmentContainer;
