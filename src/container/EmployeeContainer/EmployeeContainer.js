import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Employee from "../../component/Employee/Employee";
import PaginationTable from "../../component/Pagination/PaginationTable";
import { getEmployee } from "../../redux/actions/employee";
import { dataFilter, getPage } from "../../redux/actions/pagination";

export const EmployeeContainer = () => {
  const page = useSelector((state) => state.pagination.currentPage);
  const perPage = useSelector((state) => state.pagination.perPage);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getEmployee(1, perPage, ""));
    dispatch(getPage(1));
    dispatch(dataFilter(""));
  }, []);

  return (
    <div>
      <Employee />
      <PaginationTable />
    </div>
  );
};

export default EmployeeContainer;
